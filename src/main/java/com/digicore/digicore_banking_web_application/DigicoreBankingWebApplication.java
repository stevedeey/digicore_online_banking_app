package com.digicore.digicore_banking_web_application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigicoreBankingWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(DigicoreBankingWebApplication.class, args);
    }

}
